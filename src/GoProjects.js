const Nightmare = require('nightmare');
const vo = require('vo');
const config = require('./config');
  const nightmare = new Nightmare(config.NIGHTMARE.PRODUCTION).useragent(config.USER_AGENT);

vo(run)(function(err) {
  if (err) throw err
});

// from https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

function* run() {
  var linkList = [
    'https://www.imdb.com/title/tt9197726/fullcredits', // For the People
    'https://www.imdb.com/title/tt4463842/fullcredits', // Scandal Diego MuNoz
    'https://www.imdb.com/title/tt7461642/combined',    // Bold & Beautiful
    'https://www.imdb.com/title/tt7553366/reference',   // NCIS
    'https://www.imdb.com/title/tt1918476/',            // Y&R 1
    'https://www.imdb.com/title/tt7917848',             // Labor (ugh)
    'https://www.imdb.com/title/tt1675866/reference',   // Double Negative
    'https://www.imdb.com/title/tt3978288/',            // Criminal Criminals
    'https://www.imdb.com/title/tt6363770/',            // American Housewife
    'https://www.imdb.com/title/tt6319676/',            // Toenail
    'https://www.imdb.com/title/tt4394104/',            // Comfort
    'https://www.imdb.com/title/tt4667544/fullcredits', // Agent Carter Lady
    'https://www.imdb.com/title/tt4125408/fullcredits', // Agent Carter Valediction
    'https://www.imdb.com/title/tt5059590/fullcredits', // Scandal Rasputin
    'https://www.imdb.com/title/tt4592038/fullcredits', // Scandal Yes
    'https://www.imdb.com/title/tt1989368/reference',   // Y&R 2
    'https://www.imdb.com/title/tt2482804/fullcredits', // Scandal Happy Birthday
    'https://www.imdb.com/title/tt3079090/',            // Lay In Wait
    'https://www.imdb.com/title/tt4124016/fullcredits', // Marry Me
    'https://www.imdb.com/title/tt4079620/fullcredits', // Jane the Virgin
    'https://www.imdb.com/title/tt4037850/fullcredits', // Faking It
    'https://www.imdb.com/title/tt1552105/',            // Look in the Mirror
    'https://www.imdb.com/title/tt2098041/fullcredits', // CSI Miami
    'https://www.imdb.com/title/tt2204093/reference',   // Desperate Housewives
    'https://www.imdb.com/title/tt2061522/reference',   // Y&R 3
    'https://www.imdb.com/title/tt1463804/fullcredits', // 24
    'https://www.imdb.com/title/tt0997122/fullcredits', // Brotherhood True love
    'https://www.imdb.com/title/tt2055112/',            // Itty P & DJ Model T
    'https://www.imdb.com/title/tt2718006/',            // Hipster Backlash
    'https://www.imdb.com/title/tt2055488/reference',   // Y&R 4
    'https://www.imdb.com/title/tt8986212/',            // Silver Moon
    'https://www.imdb.com/title/tt1479847/fullcredits'  // 2012: Supernova
  ];
  shuffle(linkList);

  // click on them all
  yield* forEach(linkList, clickRoutine);

  // that is all
  yield nightmare
    .end()
    .then(function (result) {
        console.log(result)
    })
    .catch(function (error) {
        console.error('ERROR:', error);
    });
}

function* clickRoutine(item) {
  function aFewSeconds() {
    var randomSeconds = Math.floor(Math.random() * 25000) + 5000; // between 5 and 25 seconds, to appear more human
    console.log('Will sleep for ' + randomSeconds + ' ms.', item);
    return randomSeconds;
  }

  yield nightmare
    .goto(item)
    .wait(aFewSeconds())
    .click('a[href*="nm2825198"]')
    .back()
    .wait(2000);
}

function* forEach(arr, fn) { // NEEDED BECAUSE FOREACH DOESN'T WORK IN GENERATORS
  let i;
  var results = [];
  for (i = 0; i < arr.length; i++) {
    results.push(yield * fn(arr[i]));
  }
  return results;
}

const Nightmare = require('nightmare');
const vo = require('vo');
const config = require('./config');
const nightmare = new Nightmare(config.NIGHTMARE.PRODUCTION).useragent(config.USER_AGENT);

vo(run)(function(err) {
  if (err) throw err
});

function* run() {
  var linkList = yield nightmare
  // google me, go to imdb link
    .goto(config.START)
    .click('a[href="https://www.imdb.com/name/nm2825198/"]')
    .wait('#actor-tt0457229')

  // go to listing by job
    .select('#filmoform > select', '?nmdp=1')
    .wait(5000)

  // expand for jobs over 5 episodes
    .evaluate(() => {
      const toggleList = document.querySelectorAll('a[onclick*="toggle"]');
      Array.from(toggleList).forEach((el) => {
        var event = document.createEvent('MouseEvent');
        event.initEvent('click', true, true);
        el.dispatchEvent(event);
      });
    })
    .wait(5000)

  // get unique links for all these jobs
    .evaluate(() => {
      // from https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
      const shuffle = array => {
        var currentIndex = array.length,
          temporaryValue,
          randomIndex;
        // While there remain elements to shuffle...
        while (0 !== currentIndex) {
          // Pick a remaining element...
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          // And swap it with the current element.
          temporaryValue = array[currentIndex];
          array[currentIndex] = array[randomIndex];
          array[randomIndex] = temporaryValue;
        }
        return array;
      };

      const nodeList = document.querySelectorAll(
        "#filmography > div:nth-child(2) a"
      );
      const elements = Array.from(nodeList, el => {
        var parser = document.createElement('a');
        parser.href = el.href;
        return parser.pathname;
      });
      const uniqueLinks = elements.filter((item, pos) => {
        return elements.indexOf(item) == pos;
      });
      return shuffle(uniqueLinks);
    });

  // click on them all
  yield* forEach(linkList, clickRoutine);

  // that is all
  yield nightmare
    .end()
    .then(function (result) {
        console.log('RESULT:', result)
    })
    .catch(function (error) {
        console.error('ERROR:', error);
    });
}

function* clickRoutine(item) {
  if (item.indexOf("tt0092325") > -1) {
    return; // skip Bold & Beautiful - big, long page causes problems
  }
  function aFewSeconds() {
    var randomSeconds = Math.floor(Math.random() * 2000) + 800; // between .8 and 2.8 seconds, to appear slightly human
    console.log('Will wait for' + randomSeconds + 'ms before clicking', item);
    return randomSeconds;
  }

  yield nightmare
    // .goto(item)
    .click('a[href*="' + item + '"]')
    .wait(aFewSeconds())
    .cookies.clearAll()
    .back()
    .wait(2000)
    .evaluate(() => {
      const toggleList = document.querySelectorAll('a[onclick*="toggle"]');
      Array.from(toggleList).forEach((el) => {
        var event = document.createEvent('MouseEvent');
        event.initEvent('click', true, true);
        el.dispatchEvent(event);
      });
    })
    .wait(5000);
}

function* forEach(arr, fn) { // NEEDED BECAUSE FOREACH DOESN'T WORK IN GENERATORS
  let i;
  var results = [];
  for (i = 0; i < arr.length; i++) {
    results.push(yield * fn(arr[i]));
  }
  return results;
}

const Nightmare = require('nightmare');
const vo = require('vo');
const config = require('./config');
const nightmare = new Nightmare(config.NIGHTMARE.PRODUCTION).useragent(config.USER_AGENT);

// from https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

vo(run)(function(err) {
  if (err) throw err
});

function* run() {
  var linkList = [
    config.START,
    'http://kevinashworth.com/about/',
    'http://www.kevinashworth.com/about/',
    'http://www.kevinashworth.com/about/',
    'http://kevinashworth.com/resume/',
    'http://www.kevinashworth.com/resume/',
    'http://www.kevinashworth.com/resume/',
    // 'https://twitter.com/kevinashworth',
    'https://search.yahoo.com/search?p=kevin+ashworth+actor',
    'https://www.bing.com/search?q=kevin+ashworth+actor',
    'https://www.youtube.com/channel/UCbgDEIEjifgVvvRXjvkHqQw',
    'https://duckduckgo.com/?q=kevin+ashworth+actor&t=hd&ia=web',
    'https://vimeo.com/kevinashworth',
    'http://kevinashworth.blogspot.com/',
    'https://24.fandom.com/wiki/Kevin_Ashworth',
    'https://www.startpage.com/do/asearch?q=kevin+ashworth+imdb',
    'https://www.baidu.com/s?wd=kevin%20ashworth',
    'https://www.ecosia.org/search?q=kevin+ashworth+imdb',
    'https://www.givero.com/search?q=kevin+ashworth+imdb',
    'https://www.qwant.com/?q=kevin%20ashworth%20imdb&t=web',
    'https://www.qwant.com/?q=kevin%20ashworth%20imdb',
    'https://about.me/kevinashworth',
    'https://marvelcinematicuniverse.fandom.com/wiki/Kevin_Ashworth',
    'https://www.google.com/search?q=kevin+ashworth+actor',
    // 'https://www.instagram.com/iamkevinashworth/',
    // why not
    'https://duckduckgo.com/?q=Craig+Michaelson+actor',
    'https://duckduckgo.com/?q=Darin+Toonder+actor',
    'https://duckduckgo.com/?q=Danielle+Motley+actor+imdb',
  ];
  shuffle(linkList);

  // click on them all
  yield* forEach(linkList, clickRoutine);

  // that is all
  yield nightmare
    .end()
    .then(function (result) {
        console.log(result)
    })
    .catch(function (error) {
        console.error('ERROR:', error);
    });
}

function* clickRoutine(item) {
  function aFewSeconds() {
    var randomSeconds = Math.floor(Math.random() * 25000) + 5000; // between 5 and 25 seconds, to appear more human
    console.log('Will sleep for ' + randomSeconds + ' ms.', item);
    return randomSeconds;
  }

  yield nightmare
    .goto(item)
    .wait(aFewSeconds())
    .click('a[href*="imdb" i], a[title*="imdb" i]')
    .wait(2000)
    .catch((error) => {
      console.error('click failed:', error);
    });
}

function* forEach(arr, fn) { // NEEDED BECAUSE FOREACH DOESN'T WORK IN GENERATORS
  let i;
  var results = [];
  for (i = 0; i < arr.length; i++) {
    results.push(yield * fn(arr[i]));
  }
  return results;
}

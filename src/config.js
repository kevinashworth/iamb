var config = {}

function getRandomInt (min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min
}

config.START = 'https://www.google.com/search?site=&source=hp&q=kevin+ashworth+actor&oq=kevin+ashworth+actor'
config.USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_0_0) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.198 Safari/537.36'
config.NIGHTMARE = {
  DEBUG: {
    show: true,
    dock: true,
    openDevTools: {
      mode: 'detach'
    },
    waitTimeout: 50000,
    typeInterval: getRandomInt(15, 25),
    width: getRandomInt(585, 1625),
    height: getRandomInt(595, 1425)
  },
  PRODUCTION: {
    waitTimeout: 60000,
    typeInterval: getRandomInt(15, 25),
    width: getRandomInt(585, 1625),
    height: getRandomInt(595, 1425)
  }
}

console.log('NIGHTMARE.DEBUG:', config.NIGHTMARE.DEBUG)
console.log('NIGHTMARE.PRODUCTION:', config.NIGHTMARE.PRODUCTION)

module.exports = config
